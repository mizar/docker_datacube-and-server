
# DataCube and Server -  Docker image

This image contains the [Datacube](https://github.com/MizarWeb/DataCube) and [Datacube Server](https://github.com/MizarWeb/DataCubeServer) apps working together to help view cubes of spectral data.

It also creates the default folders for .fits and .nc files and downloads the cube 1342228703_M17-2_SPIRE-FTS_15.0.3244_HR_SLW_gridding_cube.fits from [HERSCHEL](http://idoc-herschel.ias.u-psud.fr/sitools/client-user/Herschel/project-index.html) that is opened at startup.
These folders can be replaced with a host one using the -v option to bind-mount one to /data as written in the following script that you can use to run a container.

## Usage

The script [run.sh](https://git.ias.u-psud.fr/mizar/docker_datacube-and-server/blob/master/run.sh) will create a local folder that can host your cubes to be viewed in the app and run a new container from this image.


You can then access the app from your navigator at  http://localhost:8000/DataCube.

## Logging
To access the container's logs do :

```bash
docker logs datacube-and-server
```

## Cleanup

To discard your container run the script [clean.sh](https://git.ias.u-psud.fr/mizar/docker_datacube-and-server/blob/master/clean.sh)
