#!/usr/bin/env bash

WORKSPACE_PATH=$PWD/datacube_workspace

docker run --name datacube-and-server \
            -d \
            -p 8000:4200 \
            -p 8081:8081 \
            -v $WORKSPACE_PATH:/data \
            idocias/datacube-and-server:latest

echo "Please launch : http://localhost:8000/DataCube"